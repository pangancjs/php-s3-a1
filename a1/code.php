<?php  
	class Product {
		public $name;
		public $price;
		public $shortDescription;
		public $stockNo;
		public $category;
		

		public function __construct($nameValue, $priceValue, $shortDescriptionValue,$categoryValue,$stockNoValue){
			//$this keyword is a reserved keyword in PHP that refers to class itself.
			$this->name = $nameValue;
			$this->price = $priceValue;
			$this->shortDescription = $shortDescriptionValue;
			$this->stockNo = $stockNoValue;
			$this->category = $categoryValue;
			
		}

	public function getProduct(){
		return "The product has a name of $this->name and its price is $this->price, and the stock no is $this->stockNo";
	}
	
	public function setStocksProduct($stockNoValue) {
			$this->stockNo = $stockNoValue;
		}

	}
	$newProduct = new Product('Xioami Mi Monitor',22,000.00,'Good for gaming and for coding',5,'computer-periperals');


	class Mobile extends Product {
		public function getMobile(){
		return "Our latest mobile is $this->name with a cheapest price of $this->price";
	}
		public function setStocksMobile($stockNoValue) {
			$this->stockNo = $stockNoValue;
			return "Mobile Stock: $this->stockNo";
		}
	}
	class Computer extends Product{
		public function getComputer(){
		return "Our newest computer is $this->name and its base price is $this->price";
	}
	public function setComputerCat($categoryValue) {
			$this->category = $categoryValue;
			return "Computer Category: $this->category";
			
		}
	}

	$newMobile = new Mobile('Xioami Redmi Note 10 pro',
				'13590.00','Latest Xioami phone ever made',10, 'mobiles and electronics');
	$newComputer = new Computer('HP Business Laptop',29000.00,'HP Laptop only made for business',10,'laptops and computer');
?>