<?php 
$buildingObj = (object)[
	'name' => 'Caswynn Bldg',
	'floors' => 8,
	'address' => (object)[
		'barangay' => 'Sacred Heart',
		'city' => 'Quezon City',
		'country' => 'Philippines'
	]
];

	class Building { //creates a blueprint/base class of our Building
		//public keyword - visibility level of how the methods/properties can be accessed by the outsiders
		//public, private, protected
		//protected - allows the children/subclass or itself to access and inherit its the methods/properties, however if the outsiders wants to have a direct access to the methods/properties it will 
		//private - disables the direct access and also disable the inhertance to its child classes;
		//declaration of properties of a Building
		// private $name; 
		// protected $name; 
		public $name; //publicly, the $name, $floors and $address  are visible to the outsiders
		public $floors;
		public $address;

		//Constructor Functions - special type of functions which is called automatically whenever we a instantiating an object (creating an instances of a class)
		public function __construct($nameValue, $floorsValue, $addressValue){
			//$this keyword is a reserved keyword in PHP that refers to class itself.
			$this->name = $nameValue;
			$this->floors = $floorsValue;
			$this->address = $addressValue;
		}
		//encapsulation - encapsulate the process and at same time these are the getters and setters of an object (Access Modifiers)
		public function getBuildingDetails(){ //once a function is created inside a class, they are now methods
			//returns the details of the instantiated object
			return "$this->name, $this->floors and $this->address";
		}

		public function getName(){ //getter
			return $this->name;
		}

		public function getFloors() {
			return $this->floors;
		}

		public function getAddress() {
			return $this->address;
		}

		public function setFloors($floorsValue) { //setters / update
			$this->floors = $floorsValue;
		}
	}
	//instantiate an object using the class Building
	$newBuilding = new Building('Enzo Bldg', 5, 'Buendia Avenue, Makati City, Philippines');
	// $newBuilding->getFloors();

	class Condominium extends Building { //Inheritance
		//Condominium class inherits the properties/methods of the base class Building
		//It means that condominiums also have name, floors and address just like a regular building

		//Polymorphism
		public function getBuildingDetails() {
			return "These are the condominium details: name $this->name, floors $this->floors, address $this->address";
		}
	}


	$newCondoUnit = new Condominium('Avida Land', 120, 'Taguig, Philippines');

	/* 

		Mini Activity:

		1. Create a class called Person with the following properties:

			first name, middle name, last name, age, birthday

		2. Instantiate an object person with below details:

			Peter Masipag Penduko, 28, 01-01-1993

		3. Create a getter and setter for ff properties of an object:

			firstname, middlename and lastname

		4. Change the name of Peter Masipag Penduko into "Pedro Tamad Penduko"
		5. Print it on the index.php on a paragraph element

	5mins (8:55pm)

	*/

	class Person {
		public $fName;
		public $mName;
		public $lName;
		public $age;
		public $birthday;

		public function __construct($fName, $mName, $lName, $age, $birthday){
			$this->fName = $fName;
			$this->mName = $mName;
			$this->lName = $lName;
			$this->age = $age;
			$this->birthday = $birthday;
		}

		public function getfName(){
			return $this->fName;
		}

		public function getmName(){
			return $this->mName;
		}
		public function getlName(){
			return $this->lName;
		}

		public function setfName($fName){
			$this->fName = $fName;
		}

		public function setmName($mName){
			$this->mName = $mName;
		}

		public function setlName($lName){
			$this->lName = $lName;
		}
	}  
//Peter Masipag Penduko, 28, 01-01-1993

	$person1 = new Person ("Peter", "Masipag", "Penduko",28, "01-01-1993");


	?>


	<!-- 
		let person = {
			fname: Juan,
			lname: Dela Cruz
		}

	person.fname = Pedro

	 -->
