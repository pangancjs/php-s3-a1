<?php require './code.php'; ?>

<!DOCTYPE html>
<html>
<head>
    <title>PHP Classes and Objects</title>
    <link rel="stylesheet" href="./style.css">
</head>
<body>
    <h1 id="heading">PHP Classes and Objects</h1>

    <?php echo $newCondoUnit->getName(); ?>
    <pre>
        <?php 
            print_r($buildingObj);
         ?>
    </pre>
    <pre>
        <?php print_r($newBuilding); ?>
    </pre>
    <pre>
        <?php print_r($person1); ?>
    </pre>
    <p>
        <?php echo $newBuilding->getBuildingDetails(); ?>
    </p>
    <?php 
        $newBuilding->setFloors(6);
     ?>
    <ul>
        <li>
            Building name: <?php echo $newBuilding->getName(); ?>
        </li>
        <li>
            Floor no.: <?php echo $newBuilding->getFloors(); ?>
        </li>
        <li>
            Address: <?php echo $newBuilding->getAddress(); ?>
        </li>
    </ul>

    <?php 
        $person1->setfName("Pedro");
        $person1->setmName("Tamad");
        $person1->setlName("Penduko");
     ?>
     <p>
         Successfully changed the name of 
         <?php echo $person1->getfName(); ?>
         <?php echo $person1->getmName(); ?>
         <?php echo $person1->getlName(); ?>
     </p>

     <hr>
     <p>
         <?php echo $newCondoUnit->getBuildingDetails(); ?>
     </p>
</body>
</html>